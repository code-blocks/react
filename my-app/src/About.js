import React from "react";

const About = (props) => {
  console.log(props);
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h2>This is About Page</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <h3>
              {props.name}-{props.age}
            </h3>
          </div>
          <div className="col-md-6">{props.children}</div>
        </div>
      </div>
    </div>
  );
};

export default About;
