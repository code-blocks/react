import React, {Component} from 'react';

class Test5 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname: '',
      emailaddress: '',
      phonenumber: '',
      error: {
        fullname: '',
        emailaddress: ''
      }
    };
  }

  changeHandler = (event) => {
    let inputFieldName = event.target.name;
    let inputFieldValue = event.target.value;
    let errorObj = this.state.error;

    if (event.target.dataset.validation) {
      if (inputFieldValue) {
        errorObj[inputFieldName] = '';
      } else {
        errorObj[inputFieldName] = event.target.dataset.validationMassage;
      }
    }

    if (event.target.dataset.validation === 'email') {
      let patt = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/;
      if (patt.test(inputFieldValue)) {
        errorObj[inputFieldName] = '';
      } else {
        errorObj[inputFieldName] = event.target.dataset.validationMassage;
      }
    }

    this.setState({errorObj, [inputFieldName]: inputFieldValue})

  }

  submitHandler = (event) => {
    event.preventDefault();
    console.log(this.state);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.submitHandler}>
          <div>
            <input
              type="text"
              data-validation
              data-validation-massage="This field is requered"
              name="fullname"
              value={this.state.fullname}
              onChange={this.changeHandler}/> {this.state.error.fullname && <div>{this.state.error.fullname}</div>}
          </div>
          <div>
            <input
              type="email"
              data-validation='email'
              data-validation-massage="Please enter valid email"
              name="emailaddress"
              value={this.state.emailaddress}
              onChange={this.changeHandler}/> {this.state.error.emailaddress && <div>{this.state.error.emailaddress}</div>}
          </div>
          <div>
            <input
              type="text"
              name="phonenumber"
              value={this.state.phonenumber}
              onChange={this.changeHandler}/>
          </div>
          <div>
            <button type="submit">Call Me</button>
          </div>
        </form>
      </div>
    );
  }
}

export default Test5;
