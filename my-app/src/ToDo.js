import React, { Component } from 'react';
import delIcon from './delete-16.png';

class ToDo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todolists:[{item:'item 1',completed:true},{item:'item 2',completed:false}],
            todolisttxt:'',
            todolisttxtError:''
        };
    }

    todolistTxtHandler=(event)=>{
        let currentTodoTxtVal=event.target.value;
        this.setState({todolisttxt:currentTodoTxtVal})
        if(currentTodoTxtVal){
            this.setState({todolisttxtError:''})
        }else{
            this.setState({todolisttxtError:'Please enter the ToDo task...'})
        }
    }
    todolistAddHandler=(event)=>{
        let currentTodoTxt=this.state.todolisttxt;
        if(currentTodoTxt){
            let todolistsArr=this.state.todolists;
            todolistsArr.push({item:currentTodoTxt,completed:false})
            this.setState({todolistsArr,todolisttxt:''})
        }else{
            this.setState({todolisttxtError:'Please enter the ToDo task...'})
        }
    }

    todolistDelHandler=(id)=>{
        let todolistsArr=this.state.todolists;
            todolistsArr.splice(id,1);
            this.setState({todolistsArr})
    }
    todolistCompleteHandler=(event,id)=>{
        let currentTodoCompleted=event.target.checked;
        let todolistsArr=this.state.todolists;
            todolistsArr[id].completed=currentTodoCompleted;
            this.setState({todolistsArr})
    }

    render() {
        let {todolists}=this.state;
        let avalableTodolists = todolists.filter(todolist => (!todolist.completed));
        return (
            <div>
                <h1>ToDo App</h1>
                <h2>ToDo List ({avalableTodolists.length})</h2>
                <div>
                <input type="text" value={this.state.todolisttxt} onChange={this.todolistTxtHandler} /><button onClick={this.todolistAddHandler}>Add</button>
        {this.state.todolisttxtError&&<div style={{fontSize:'12px', fontStyle:'italic', color:'red'}}>{this.state.todolisttxtError}</div>}
                </div>
                <ul>
                {todolists.map((itemObj,index)=>{
                    return (<li style={itemObj.completed?{textDecoration: 'line-through'}:{}} key={index}>
                        <label>
                        <input type="checkbox" checked={itemObj.completed} onChange={(e)=>{this.todolistCompleteHandler(e,index)}} /> 
                        {itemObj.item} 
                        </label>
                        <img src={delIcon} width="12" alt="" onClick={()=>{this.todolistDelHandler(index)}} />
                        </li>)
                })}
                </ul>
            </div>
        );
    }
}

export default ToDo;
