import React, { Component } from 'react';
import axios from 'axios';

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts:[],
            error:''
        };
    }
    componentDidMount(){
        let userID=this.props.match.params.id;
        axios.get(`https://jsonplaceholder.typicode.com/posts?userId=${userID}`)
        .then((response) => {
            this.setState({
                posts:response.data,
                error:(response.data.length?'':'Somting went wrong!!')
            })
        })
        .catch((error) => {
            this.setState({
                error:'Somting went wrong!!'
            })
        })
    }
    
    render() {
        let posts=this.state.posts;
        return (
            <div>
                <h2>User</h2>
                {posts.length?
                (posts.map((val,key)=>{
                    return(<div key={key}>
                        <h3>{val.title}</h3>
                        <p>{val.body}</p></div>)
                })):
                <div>{this.state.error}</div>
            }
            </div>
        );
    }
}

export default User;
