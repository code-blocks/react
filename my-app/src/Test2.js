import React, { Component } from 'react';

class Test2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: false,
            loading: false,
            email: 'manu@getit.marketing'
        };
    }



    LoginHandler = () => {
        this.setState({
            login: false,
            loading: true
        });
        setTimeout(() => {
            this.setState({
                login: true,
                loading: false
            });
        }, 3000)
    }



    render() {
        var message;
        if (this.state.login) {
            message = <div>Welcome {this.props.firstName} {this.props.lastName}</div>
        } else {
            message = <button onClick={this.LoginHandler}>Login</button>
        }
        return (
            <div>
                {message}
                {this.state.loading && <div>Loading...</div>}

            </div>

        )
        /* return (
            <div>
                {this.state.email}
                <button onClick={this.LoginHandler}>Login</button>
                {this.state.login&&<div>Welcome {this.props.firstName} {this.props.lastName}</div>}
            </div>
        ) */


        /* return (
            <div>
                {this.state.email}
                {this.state.login ? (<div>
                    Welcome {this.props.firstName} {this.props.lastName}
                </div>) :
                    (<div>
                        <button onClick={this.LoginHandler}>Login</button>
                    </div>)}
            </div>
        ) */

        /* if (this.state.login) {
            return (
                <div>{this.state.email}
            Welcome {this.props.firstName} {this.props.lastName}</div>
            )
        } else {
            return (
                <div>{this.state.email}
                    <button onClick={this.LoginHandler}>Login</button></div>
            )
        } */
        /* var message;
        if (this.state.login) {
            message = <div>Welcome {this.props.firstName} {this.props.lastName}</div>
        } else {
            message = <button onClick={this.LoginHandler}>Login</button>
        }
        return (
            <div>
                {this.state.email}
                {message}
            </div>
        ); */

    }
}

export default Test2;
