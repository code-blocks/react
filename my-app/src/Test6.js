import React, { Component } from 'react';
import editIcon from './edit-flat.png';

class Test6 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName:"Finoy",
            errorFirstName:"",
            items:[{fName:'Finoy',lName:'Francis'},{fName:'Manu',lName:'TM'},{fName:'Jithin',lName:'PV'}],
            checkboxchecked:false
        };
    }

    changeHandler=(event)=>{
        let pattern=/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        if(event.target.type=='checkbox'){
            console.log(event.target.dataset.age);
        this.setState({checkboxchecked:event.target.checked})  
        }else{
        if(!event.target.value){
            this.setState({
                errorFirstName:'This field is requered'
            })
        }else if(!pattern.test(event.target.value)){
            this.setState({
                errorFirstName:'Please enter valid email'
            })
        }else{
            this.setState({
                errorFirstName:''
            })
        }

        this.setState({
            firstName:event.target.value
        })
    }
    }
    submitHandler=(event)=>{
        event.preventDefault()
        console.log(this.state)
    }

    dataHandler=(event)=>{
        console.log(event.target.dataset);     
    }

    testMap=(val,key)=>{
        return <div key={key}>{val.fName} {val.lName}
        <img src={editIcon} width="12" alt="" data-arrindex={key} data-fullname={`${val.fName} ${val.lName}`} onClick={this.dataHandler} /></div>
    }
    
    render() {
        return (
            <div>
                {this.state.items.map(this.testMap)}
                <h2>Form Controls</h2>
                <form onSubmit={this.submitHandler}>
                    <input type="text" name="firstName" value={this.state.firstName} onChange={this.changeHandler} />
                    {<div style={{color:'red'}}>{this.state.errorFirstName}</div>}
                    {this.state.checkboxchecked&&'checked'}
                    <input type="checkbox" name="dummyCheckBox" data-age={35} data-firstname="finoy" checked={this.state.checkboxchecked} onChange={this.changeHandler} />                    <button type="submit">Submit</button>
                </form>
            </div>
        );
    }
}

export default Test6;
