import React, { Component } from 'react';

class Test3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...this.props,
            email: 'finoyhere@gmail.com'
        };
    }

    testFn = () => {
        this.setState({
            firstName: 'Testing'
        })
    }

    render() {

        return (
            <div>

                Welcome {this.state.firstName} {this.state.lastName}
                {this.state.children}
                {this.state.email}
                <button onClick={this.testFn}>Click me</button>


            </div>
        );
    }
}

export default Test3;
