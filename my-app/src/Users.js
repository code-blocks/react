import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';





class Users extends Component {
constructor(props) {
    super(props);
    this.state = {
        users:[],
        error:''
    };
}

componentDidMount(){
    
    /* fetch('https://my-json-server.typicode.com/Finoy/fake-api/user')
        .then(response => response.json())
        .then(json => console.log(json)) */

        axios.get('https://my-json-server.typicode.com/Finoy/fake-api/users')
        .then((response) => {
            this.setState({
                users:response.data
            })
        })
        .catch((error) => {
            this.setState({
                error:'Somting went wrong!!'
            })
        })
        
}

    render() {
        let users=this.state.users;
        console.log(users);
        return (
            <div>
                <h2>Users</h2>
                {users.map((val,key)=>{
                    return(<div key={key}><Link to={`/users/${val.id}`}>{val.name.first} {val.name.last}</Link></div>)
                })}
            </div>
        );
    }
}

export default Users;
