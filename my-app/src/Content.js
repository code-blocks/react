import React, { Component } from 'react';

class Content extends Component {
    render() {

        return (
            <div>
                Test class comp {this.props.fn} {this.props.ln}
                {this.props.children}
            </div>
        );
    }
}

export default Content;
