import React from "react";
/* import Test1 from './Test1';
import Test2 from './Test2'; */
/* import Test4 from './Test4';
import Test6 from "./Test6";
import Test7 from "./Test7";
import ToDo from './ToDo'; */
/* import FireApp from "./FireApp";*/
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Users from "./Users";
import User from "./User";
import PageNotFound from "./PageNotFound";
import "./App.scss";
import Header from "./Header";
import { Container, Row, Col } from "react-bootstrap";

function App() {
  const boxStyle = { background: "#ccc", fontSize: "16px" };
  return (
    <div style={{ marginBottom: "100px" }}>
      {/* <h1>Hello World</h1>
      <Test1 firstName="Finoy" lastName="Francis" age={35} adrs={{city:'Thrissur',pincode:680611}}>
        <p>This is <br />from child</p>
      </Test1>
      <Test1 firstName="Thomas" age={40}  />
      <Test1 firstName="Jithin" lastName="PV" age={28} sty={{color:'red','fontSize':'24px'}} />
      <Test1 firstName="Manu" age={26} adrs={{city:'Thrissur',pincode:680004}} d={['11111222','77777']}>
        <p>Typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      </Test1>



      <Test2 firstName="Finoy" lastName="Francis">
        <p>This is <br />from child in class comp</p>
      </Test2>

      <Test4 />

      <Test6 />
      <Test7 /> */}
      {/* <ToDo /> */}
      {/* <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/todo">ToDo</a></li>
        </ul> */}

      {/* <BrowserRouter>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/Test6">Test6</Link>
          </li>
        </ul>
        <Route path="/" component={Test7} exact />
        <Route path="/Test6" component={Test6} />
        <Route path="/fire-app" component={FireApp} />
        <Route path="/todo-finoy" component={ToDo} />
      </BrowserRouter> */}

      {/* <Test4 /> */}

      <BrowserRouter>
        <Header />
        <Container>
          <Row>
            <Col lg={6}>
              <Switch>
                <Route path="/" component={Home} exact />
                <Route path="/about">
                  <About name="Thomas" age={40}>
                    <p>This is about page child elements</p>
                  </About>
                </Route>
                <Route path="/users" component={Users} exact />
                <Route path="/users/:id" component={User} />
                <Route component={PageNotFound} />
              </Switch>
            </Col>
          </Row>
        </Container>
      </BrowserRouter>
      <div className="box1" style={boxStyle}>
      </div>
    </div>
  );
}

export default App;
