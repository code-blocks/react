import React, { Component } from 'react';

class Test4 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name:'Finoy'
        };
        console.log('1. Initialization');
    }

    componentWillMount(){
        console.log('2. componentWillMount');
        
    }

    
    LoginHandler = () => {
        this.setState({name:'Thomas'})
    }
    
    render() {
        console.log('3. render');
        return (
            <div>
                This session is class component Life cycle <br />
                {this.state.name}
                <button onClick={this.LoginHandler}>Change User</button>
            </div>
        );
    }
    
    componentDidMount(){
        console.log('4. componentDidMount');
    }

    componentWillUpdate(){
        console.log('5. componentWillUpdate');
    }

    componentDidUpdate(){
        console.log('6. componentDidUpdate');
    }

    
}

export default Test4;
