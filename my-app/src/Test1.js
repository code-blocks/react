import React from 'react';

const Test1 = (props) => {
    console.log(props);
    var data;
    if(props.adrs){
        data=props.adrs.city;
    }
    var arr=[];
    if(props.d){
        arr=props.d;
    }
    return (
        <div style={props.sty}>
            Welcome {props.firstName} {props.lastName} {props.age} {data}
            {arr[0]}
            {props.children}
        </div>
    );
}

export default Test1;
